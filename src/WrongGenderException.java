public class WrongGenderException extends Exception {
    public WrongGenderException(String s) {
        super(s);
    }
}
