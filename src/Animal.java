import java.util.Random;
import java.util.Set;

public abstract class Animal {

    private final Gender gender;
    private final Diet diet;
    private final Food[] foods;
    private final double temp;
    private final int litter;

    public Animal(Gender gender, Diet diet, Food[] foods, double temp, int litter) {
        this.gender = gender;
        this.diet = diet;
        this.foods = foods;
        this.temp = temp;
        this.litter = litter;
    }

    public abstract Set<Animal> giveBirth() throws WrongGenderException;
    public boolean heartbeat() {
        Random r = new Random();
        return (r.nextInt(10) < 8);
    }

    public Gender getGender() {
        return gender;
    }

    public Diet getDiet() {
        return diet;
    }

    public Food[] getFoods() {
        return foods;
    }

    public double getTemp() {
        return temp;
    }

    public int getLitter() {
        return litter;
    }
}
