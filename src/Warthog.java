import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Random;

public class Warthog extends Animal {

    private static final Food[] foods;

    static {
        foods = new Food[]{Food.GRASS, Food.EGGS, Food.FRUIT};
    }

    public Warthog (Gender gender) {
        super(gender, Diet.OMNIVORE, foods, 50.0,8);
    }

    @Override
    public Set<Animal> giveBirth() throws WrongGenderException {
        if (getGender() == Gender.MALE) {
            throw new WrongGenderException("Males cannot give birth");
        } else {
            HashSet<Animal> children = new HashSet<>();
            if (randomPercentage(1)) {
                int numChildren = genNumOutOfX(getLitter()) + 1;
                for (int i = 0; i < numChildren; i++) {
                    children.add(new Warthog(randGender()));
                }
            }
            return children;
        }
    }

    private boolean randomPercentage(int bound) {
        Random r = new Random();
        return (r.nextInt(10) < bound);
    }

    private int genNumOutOfX(int x) {
        Random r = new Random();
        return r.nextInt(x);
    }

    private Gender randGender() {
        if (genNumOutOfX(2) == 0) {
            return Gender.MALE;
        }
        return Gender.FEMALE;
    }

}

