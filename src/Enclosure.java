import java.util.HashSet;
import java.util.Set;

public class Enclosure<T> {

    Set<T> animals;
    int maxSize;

    public Enclosure(int maxSize) {
        animals = new HashSet<>();
        this.maxSize = maxSize;
    }

    public boolean addAnimal(T animal) {
        if (animals.size() >= maxSize) {
            return false;
        }
        animals.add(animal);
        return true;
    }

}
